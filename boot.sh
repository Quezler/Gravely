#!/usr/bin/env bash
for patch in ./vendor/Quezler/Gravely/src/Assets/Patches/*.{patch,diff}
do
	git apply ${patch}
done
