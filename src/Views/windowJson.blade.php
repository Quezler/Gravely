<script>
@foreach((new \Quezler\Gravely\Events\WindowJsonEvent)->toArray() as $namespace => $array)
    window.{{ $namespace }} = {!! \GuzzleHttp\json_encode($array) !!};
@endforeach
</script>
