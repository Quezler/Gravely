<?php

namespace Quezler\Gravely\Helpers;

/**
 * Stuff that spans other stuff.
 *
 * Class Span
 * @package Quezler\Gravely\Helpers
 */
class Span
{
    /**
     * Return the first power of 2 that is bigger or equal to its argument.
     *
     * 63 > 64
     * 64 > 64
     * 65 > 128
     *
     * @param integer $count
     * @return integer span
     */
    public static function binary($count) {
        $i = 1;
        $result = 0;
        while($result < $count) {
            $result = pow(2, $i++);
        }
        return $result;
    }
}
