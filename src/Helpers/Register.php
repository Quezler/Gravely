<?php

namespace Quezler\Gravely\Helpers;

use Illuminate\Support\ServiceProvider;

/**
 * Register stuff.
 *
 * Class Register
 * @package Quezler\Gravely\Helpers
 */
class Register
{
    /**
     * Add service provider to ./config/app.php if it is not in there.
     *
     * @param $sp ServiceProvider::class
     */
    public static function serviceProvider($sp) {
        $namespace = "$sp::class";

        $config = file_get_contents('./config/app.php');

        if (strpos($config, $namespace) !== false) {
            return; // is present in file.
        }

        $insertWhenEmpty = false;
        $lines = explode(PHP_EOL, $config);

        foreach ($lines as $number => $line) {

            // find package service providers
            if ($line == '         * Package Service Providers...') {
                // mark next white line as location to insert the service provider
                $insertWhenEmpty = true;
            }

            // v pre | post ^ | https://github.com/laravel/laravel/commit/936addceefec2093bc8d010d2cba9a361dae2a0e

            // find application service providers
            if ($line == '         * Application Service Providers...') {
                // mark next white line as location to insert the service provider
                $insertWhenEmpty = true;
            }

            // insert service provider on marked empty line
            if (empty($line) && $insertWhenEmpty) {

                // pad, comma & newline
                $lines[$number] = "        $namespace,\n";
                // put back
                file_put_contents('./config/app.php', implode(PHP_EOL, $lines));
                // break function
                break;
            }
        }
    }
}
