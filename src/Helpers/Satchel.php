<?php

namespace Quezler\Gravely\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Provides eyecandy enhancements.
 *
 * Class Satchel
 * @package app\Helpers
 */
class Satchel
{
    /**
     * Wrapper for @see Paginator that is bigger or equal than its content,
     * returns a response for resource controller typehinting.
     *
     * @param Collection $items
     * @return \Illuminate\Http\Response
     */
    public static function paginator(Collection $items) {
        return response()->json(
            new Paginator(
                $items, Span::binary($items->count())
            )
        );
    }
}
