<?php

namespace Quezler\Gravely\Helpers;

use Illuminate\Support\Facades\File;

/**
 * Functions related to storing and retrieving related to disk interactions.
 *
 * Class Disk
 * @package Quezler\Gravely\Helpers
 */
class Disk
{
    /**
     * Only write to file if the current and new content do not match.
     *
     * Just like the origin of pootis, this function can stop halfway.
     *
     * - avoid infinite loops in file watchers
     *
     * @param $file
     * @param $content
     */
    public static function pootis($file, $content) {
        if (!File::exists($file) || File::get($file) !== $content) {
             File::put($file, $content);
        }
    }
}
