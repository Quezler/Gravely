<?php

namespace Quezler\Gravely\Helpers;

use Illuminate\Support\Debug\Dumper;
use League\Flysystem\FileNotFoundException;
use Quezler\Gravely\Providers\GravelyServiceProvider;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Git patch related php functions.
 *
 * Class GitPatch
 * @package Quezler\Gravely\Helpers
 * @deprecated
 */
class GitPatch
{
    public static function apply(SplFileInfo $patch) {

        if (file_exists($patch->getPathname(). 'ed')) {
            return; // this patch was marked as applied
        }

        exec('git apply '. escapeshellarg($patch->getPathname()) . ' 2>&1', $output);

        // $output[ ]

        // $output[0] 'error: .foo.txt: already exists in working directory'

        // $output[0] 'error: patch failed: app/Exceptions/Handler.php:20'
        // $output[1] 'error: app/Exceptions/Handler.php: patch does not apply'

        // todo: handle output.

        (new Dumper())->dump($output);

//        if (empty($output)) {
            touch($patch->getPathname(). 'ed'); // mark patch as patch[ed]
//        }
    }

    /**
     * Locate a patch file.
     *
     * @param $name
     * @return SplFileInfo
     * @throws FileNotFoundException
     */
    public static function locate($name) {
        $patches = GravelyServiceProvider::src('/Assets/Patches');
        $patch = "$patches/$name.patch";

        if (file_exists($patch)) {
            return new SplFileInfo($patch, null, null);
        } else {
            throw new FileNotFoundException($patch);
        }
    }
}
