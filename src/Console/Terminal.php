<?php

namespace Quezler\Gravely\Console;

use Symfony\Component\Console\Helper\Table;

trait Terminal
{
    private function header()
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $table = new Table($this->output);
        $table
            ->setHeaders(array('<fg=blue>'.__CLASS__.'</>'))
        ;
        $table->render();
    }
}
