<?php

namespace Quezler\Gravely\Console\Commands;

use Symfony\Component\Console\Helper\Table;

class CronCompileCommand extends GravelyCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gravely:cron:compile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Beep beep i'm a sheep.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        (new CompileSassCommand)->run(new ArrayInput([]), $this->output);
//        (new CompileSassCommand)->run($this->output);
//        (new CompileSassCommand)->handle();
//        $this->call((new \ReflectionClass(CompileSassCommand::class))->set);
//        $this->call(CompileVueCommand::class);

//        $this->call('gravely:compile:sass');
//        $this->call('gravely:compile:vue');

        foreach ([CompileSassCommand::class, CompileVueCommand::class] as $class) {
            $this->header($class);
            /** @noinspection PhpUndefinedMethodInspection */
            $this->call((new $class)->getCommand()); // todo: check if class implements GravelyCommand
        }
    }

    private function header($class)
    {
        $table = new Table($this->output);
        $table
            ->setHeaders(array("<fg=blue>$class</>"))
        ;
        $table->render();
    }
}
