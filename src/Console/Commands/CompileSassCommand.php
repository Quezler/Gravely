<?php

namespace Quezler\Gravely\Console\Commands;

use Illuminate\Support\Facades\File;
use Quezler\Gravely\Helpers\Disk;
use Symfony\Component\Finder\SplFileInfo;

class CompileSassCommand extends GravelyCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gravely:compile:sass';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile sass partials.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sassAll();
    }

    private function sassAll()
    {
        $this->line('<fg=blue>'.__FUNCTION__.'</>');

        $alls = [];

        /**
         * @var SplFileInfo[] $files
         */
        $sass = resource_path('assets/sass');
        /** @noinspection PhpUndefinedMethodInspection */
        $files = File::allFiles($sass);

        foreach ($files as $file) {
            $this->info($file->getRelativePathname());
            if (!$file->getRelativePath()) {
                $this->comment('exists in root, skipping.');
                continue; // file sits in sass root directory.
            }

            // mark content to be cleared if it receives no additional values.
            if ($file->getFilename() == '_all.scss') {
                $alls[$file->getRelativePath().'/_all.scss'][] = null;
                $this->comment('is an _all, skipping.');

                continue;
            }

            if (!preg_match("/_((?!all).*).scss/", $file->getFilename(), $match)) {
                $this->comment('not a valid partial, skipping.');
                continue; // file is not a sass partial.
            } else {
                $partial = $match[1];
            }

            $this->comment('marked as partial.');
            $alls[$file->getRelativePath().'/_all.scss'][] = '@import "'. $partial .'";';
        }

        foreach ($alls as $all => $lines) {

            // remove clear content mark if this all found partial(s).
            if (count($lines) > 1) {
                $lines = array_filter($lines);
            }

            $lines = array_merge(['// '. __CLASS__, null], $lines, [null]);

            $path = "$sass/$all";
            $text = implode(PHP_EOL, $lines);

            // remove all but last whiteline
            $text = rtrim($text, PHP_EOL);
            $text .= PHP_EOL;

            Disk::pootis($path, $text);
        }
    }
}
