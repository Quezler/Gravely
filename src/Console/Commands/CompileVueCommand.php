<?php

namespace Quezler\Gravely\Console\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Quezler\AsciiExtended\Ascii;
use Quezler\Gravely\Helpers\Disk;
use Symfony\Component\Finder\SplFileInfo;

class CompileVueCommand extends GravelyCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gravely:compile:vue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile vue components.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->compile();
    }

    private function compile($lines = [])
    {
        $this->line('<fg=blue>'.__FUNCTION__.'</>');

        /**
         * @var SplFileInfo[] $components
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $components = File::allFiles(resource_path('/assets/js/components/'), true);

        foreach ($components as $component) {
            if (!$this->isVueFile($component)) {
                $this->comment($component->getRelativePathname());
                continue;
            }
            $this->info($component->getRelativePathname());

            $path = explode(DIRECTORY_SEPARATOR, substr($component->getRelativePathname(), 0, -4));

            $lines[] = "import ". implode(null, array_map('ucfirst', $path)) ." from '../components/". $component->getRelativePathname() ."';";
        }

        usort($lines, function ($a, $b) {
            return strlen($a)-strlen($b);
        });
        // vue.component
        $lines[] = null;
        $lines[] = 'import Vue from \'vue\';';
        $lines[] = null;
        preg_match_all("/import (\\w*) from '..\\/components\\/(.*).vue';/U", implode("\n", $lines), $matches);
        foreach ($matches[2] as $i => $line) {
            $path = explode("/", $line);
            $path = array_map('strtolower', $path);

            $lines[] = sprintf(
                "Vue.component('%s', %s);",
                str_replace('::_', '-', implode('_', $path)),
                $matches[1][$i]
            );
        }


        // export
        preg_match_all("/import (\\w*) from/", implode("\n", $lines), $matches);

        $exports = implode(",\n    ", $matches[1]);

        $lines[] = "\nexport {\n    $exports,\n};";

        $lines = array_merge([
            '// ' . self::class,
            '',
        ], array_merge($lines, [null]));

        Disk::pootis(resource_path('assets/js/compiled/vue.component.import.js'), implode(Ascii::line_feed, $lines));
    }

    private function isVueFile(SplFileInfo $file)
    {
        return Str::endsWith($file->getFilename(), '.vue');
    }
}
