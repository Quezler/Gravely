<?php

namespace Quezler\Gravely\Console\Commands;

use Illuminate\Console\Command;

class GravelyCommand extends Command
{
    /**
     * Return command part from signature.
     *
     * @return string
     */
    public function getCommand(): string
    {
        return $this->signature; // todo: filter out the command and leave the rest of the signature
    }
}
