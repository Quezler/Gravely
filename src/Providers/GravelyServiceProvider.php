<?php

namespace Quezler\Gravely\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Quezler\Gravely\Console\Commands\CompileVueCommand;
use Quezler\Gravely\Console\Commands\CompileSassCommand;
use Quezler\Gravely\Console\Commands\CronCompileCommand;
use Quezler\Gravely\Enums\Char;
use Quezler\Gravely\Helpers\GitPatch;
use Quezler\Gravely\Middleware\HumansNeedNotApply;

class GravelyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $addroutemiddleware = $this->addRouteMiddlewareFunction();

        $router->{$addroutemiddleware}('human', HumansNeedNotApply::class);

        $this->loadViewsFrom(self::src('/Views'), 'gravely');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        if ($this->app->runningInConsole()) {
            $this->commands([
                CronCompileCommand::class,

                CompileSassCommand::class,
                CompileVueCommand::class,
            ]);
        }
    }

    /**
     * Return absolute path of Quezler\Gravely\src folder.
     *
     * @param  string  $path
     * @return string
     */
    public static function src($path = '') {
        return __DIR__ . '/..'. ($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Find the function to add named route middleware during runtime.
     *
     * @return string
     */
    private function addRouteMiddlewareFunction() {
        list($major, $minor, $patch) = array_map('intval', explode(Char::PERIOD, Application::VERSION) );

        switch ($minor) {
            case 3:
                return 'middleware';
            case 4:
            default:
                return 'aliasMiddleware';
        }
    }
}
