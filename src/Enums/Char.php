<?php

namespace Quezler\Gravely\Enums;

use Quezler\AsciiExtended\Ascii;

/**
 * Useful for e.g. making implodes and explodes look better.
 *
 * Class Char
 * @package Quezler\Gravely\Enums
 * @deprecated
 */

class Char
{
    const  PERIOD = Ascii::period;
    const  SPACE  = Ascii::space;
    const  HYPHEN = Ascii::hyphen;
    const  UNDERS = Ascii::underscore;
    const  NOT    = Ascii::not_sign;
    const  SLASH  = Ascii::slash;
}
