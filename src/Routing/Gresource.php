<?php

namespace Quezler\Gravely\Routing;

use Illuminate\Routing\ResourceRegistrar;
use Illuminate\Support\Str;
use Quezler\AsciiExtended\Ascii;
use Symfony\Component\Finder\SplFileInfo;

class Gresource extends ResourceRegistrar
{
    /**
     * Route a resource to a controller.
     *
     * @param  string  $name
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    public function register($name, $controller, array $options = [])
    {
        if (isset($options['parameters']) && ! isset($this->parameters)) {
            $this->parameters = $options['parameters'];
        }

        // If the resource name contains a slash, we will assume the developer wishes to
        // register these resource routes with a prefix so we will set that up out of
        // the box so they don't have to mess with it. Otherwise, we will continue.
        if (Str::contains($name, '/')) {
            $this->prefixedResource($name, $controller, $options);

            return;
        }

        // We need to extract the base resource from the resource name. Nested resources
        // are supported in the framework, but we need to know what name to use for a
        // place-holder on the route parameters, which should be the base resources.
        $base = $this->getResourceWildcard(last(explode('.', $name)));

        $defaults = $this->resourceDefaults;

        foreach ($this->getResourceMethods($defaults, $options, $controller) as $m) {
            $this->{'addResource'.ucfirst($m)}($name, $base, $controller, $options);
        }
    }

    /**
     * Build a set of prefixed resource routes.
     *
     * @param  string  $name
     * @param  string  $controller
     * @param  array   $options
     * @return void
     */
    protected function prefixedResource($name, $controller, array $options)
    {
        list($name, $prefix) = $this->getResourcePrefix($name);

        // We need to extract the base resource from the resource name. Nested resources
        // are supported in the framework, but we need to know what name to use for a
        // place-holder on the route parameters, which should be the base resources.
        $callback = function ($me) use ($name, $controller, $options) {
            Grouter::resource($name, $controller, $options);
        };

        return $this->router->group(compact('prefix'), $callback);
    }

    /**
     * Get the applicable resource methods.
     *
     * @param  array $defaults
     * @param  array $options
     * @param  string $controller
     * @return array
     */
    protected function getResourceMethods($defaults, $options)
    {
        $controller     = func_get_args()[2];
        $controllerPath = (new \ReflectionClass($controller))->getFileName();
        $controllerFile = new SplFileInfo($controllerPath, null, null);

        return $this->controllerCanHandle($controllerFile);

//        if (isset($options['only'])) {
//            return array_intersect($defaults, (array) $options['only']);
//        } elseif (isset($options['except'])) {
//            return array_diff($defaults, (array) $options['except']);
//        }
//
//        return $defaults;
    }

    /**
     * Detect if a controller has code to handle said method.
     *
     * If its the default stub, do not register that route.
     *
     * avg 0.0011210441589355 seconds, not bad.
     *
     * @param SplFileInfo $controller
     * @param array $bag
     * @return array
     */
    private function controllerCanHandle(SplFileInfo $controller, $bag = []) {
        $file = str_replace(Ascii::line_feed, null, $controller->getContents());
        foreach ($this->resourceDefaults as $method) {
            preg_match("/public function $method\\([^\\*]*\\)    {\\s{8}\\/\\/\\s{4}}/U", $file, $matches) ?: $bag[] = $method;
        }
        return $bag;
    }

}
