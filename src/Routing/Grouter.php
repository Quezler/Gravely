<?php

namespace Quezler\Gravely\Routing;

class Grouter
{
    /**
     * Route a resource to a controller.
     *
     * @param  string  $name
     * @param  string  $controller
     * @param  array  $options
     * @return void
     */
    public static function resource($name, $controller, $options = [])
    {
        $registrar = new Gresource(app('router'));

        $registrar->register($name, $controller, $options);
    }
}
