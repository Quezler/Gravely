<?php

namespace Quezler\Gravely\Middleware;

use Closure;

class HumansNeedNotApply
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isJson() || $request->wantsJson()) {
            return $next($request);
        }

        return response()->view('gravely::vue', [
            'component' => str_replace('.', '_',
                $request->route()->getName() ?? 'this_route_has_no_name'
            )
        ]);
    }
}
