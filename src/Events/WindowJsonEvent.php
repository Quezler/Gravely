<?php

namespace Quezler\Gravely\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Arr;
use Quezler\AsciiExtended\Ascii;
use Quezler\Gravely\Helpers\Disk;

class WindowJsonEvent implements Arrayable
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the instance as an array.
     *
     * @param array $bag
     * @return array
     */
    public function toArray($bag = [])
    {
        $returns = event(new self);

        /**
         * array:2 [▼
        0 => array:3 [▼
        "Laravel.token.csrf" => "124woElMCwMf97QXjEQOciG7Yl9Ljyuy5SUb6A7D"
        "Laravel.token.api" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9xdXJsMy5xcmVhdGl2LmZvb2JhciIsImF1ZCI6Imh0dHA6XC9cL3F1cmwzLnFyZWF0aXYuZm9vYmFyIiwiaWF0IjoxNTAxNDM1OTY ▶"
        "Laravel.token.pusher" => "5f478cd7625a50305f83"
        ]
        1 => array:1 [▼
        "Laravel.token.something" => "else"
        ]
        ]
         */

        $dottedmulti = Arr::dot($returns);

        /**
         * array:4 [▼
        "0.Laravel.token.csrf" => "124woElMCwMf97QXjEQOciG7Yl9Ljyuy5SUb6A7D"
        "0.Laravel.token.api" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9xdXJsMy5xcmVhdGl2LmZvb2JhciIsImF1ZCI6Imh0dHA6XC9cL3F1cmwzLnFyZWF0aXYuZm9vYmFyIiwiaWF0IjoxNTAxNDM1OTY ▶"
        "0.Laravel.token.pusher" => "5f478cd7625a50305f83"
        "1.Laravel.token.something" => "else"
        ]
         */

        $dottedsingle = collect($dottedmulti)->mapWithKeys(function ($value, $key) {
            $exploded = explode(Ascii::period, $key); // ["0", "Laravel", "token", "csrf"];
            array_shift($exploded); // ["Laravel", "token", "csrf"];
            return [implode(Ascii::period, $exploded) => $value]; // "Laravel.token.csrf";
        })->toArray();

        /**
         * array:4 [▼
        "Laravel.token.csrf" => "124woElMCwMf97QXjEQOciG7Yl9Ljyuy5SUb6A7D"
        "Laravel.token.api" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9xdXJsMy5xcmVhdGl2LmZvb2JhciIsImF1ZCI6Imh0dHA6XC9cL3F1cmwzLnFyZWF0aXYuZm9vYmFyIiwiaWF0IjoxNTAxNDM1OTY ▶"
        "Laravel.token.pusher" => "5f478cd7625a50305f83"
        "Laravel.token.something" => "else"
        ]
         */

        // typehint variables in javascript, but only when debug is on to avoid cluttering the system
        if (config('app.debug')) {
            $lines = [
                '// ' . self::class,
                '',
            ];
            foreach ($dottedsingle as $key => $value) {
                $lines[] = "/** @namespace window.$key */";
            }
            $lines[] = '';
            Disk::pootis(resource_path('assets/js/compiled/jsdoc.namespace.windowjson.js'), implode(Ascii::line_feed, $lines));

            // // Quezler\Gravely\Events\WindowJsonEvent

            // /** @namespace window.Laravel.token.csrf */
            // /** @namespace window.Laravel.token.api */
            // /** @namespace window.Laravel.token.pusher */
        }

        $return = [];
        foreach ($dottedsingle as $key => $value) {
            array_set($return, $key, $value);
        }

        /**
         * array:1 [▼
        "Laravel" => array:1 [▼
        "token" => array:4 [▼
        "csrf" => "124woElMCwMf97QXjEQOciG7Yl9Ljyuy5SUb6A7D"
        "api" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9xdXJsMy5xcmVhdGl2LmZvb2JhciIsImF1ZCI6Imh0dHA6XC9cL3F1cmwzLnFyZWF0aXYuZm9vYmFyIiwiaWF0IjoxNTAxNDM1OTY ▶"
        "pusher" => "5f478cd7625a50305f83"
        "something" => "else"
        ]
        ]
        ]
         */

        return $return;
    }
}
