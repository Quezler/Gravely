<?php

namespace Quezler\Gravely\Exceptions;

/**
 * Throw exception that laravel does not log.
 *
 * Primary usage: silently retrying current job.
 *
 * Class DevNullException
 * @package Quezler\Gravely\Exceptions
 */
class DevNullException extends \LogicException
{
    // (っ◕‿◕)っ
}
