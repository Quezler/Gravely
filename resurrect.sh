#!/usr/bin/env bash
#   ___                 _            ______                     _
#  / _ \ _   _  ___ ___| | ___ _ __ / / ___|_ __ __ ___   _____| |_   _
# | | | | | | |/ _ \_  / |/ _ \ '__/ / |  _| '__/ _` \ \ / / _ \ | | | |
# | |_| | |_| |  __// /| |  __/ | / /| |_| | | | (_| |\ V /  __/ | |_| |
#  \__\_\\__,_|\___/___|_|\___|_|/_/  \____|_|  \__,_| \_/ \___|_|\__, |
#                                                                 |___/

# variables
bold=$(tput bold)
normal=$(tput sgr0)

# confirmation
echo "Install Quezler/Gravely in [ ${bold}$(pwd)${normal} ]?"
read senpai

# laranew
laravel new repo
mv repo/* ./
mv repo/.[!.]* ./
rmdir repo/

# npm
npm install

# gravely
composer require Quezler/Gravely
php ./vendor/Quezler/Gravely/boot.php
sh  ./vendor/Quezler/Gravely/boot.sh
