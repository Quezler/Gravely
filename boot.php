<?php

/** @noinspection PhpIncludeInspection */
require getcwd().'/bootstrap/autoload.php';

/** @noinspection PhpParamsInspection */
Quezler\Gravely\Helpers\Register::serviceProvider(Quezler\Gravely\Providers\GravelyServiceProvider::class);
